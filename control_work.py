import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import random

def get_points():
    Points = []
    X = []
    Y = []
    Vx = []
    Vy = []
    col = []
    for t in range(100):
        X.append(random.random()*2)
        Y.append(random.random()*2)
        Vx.append((random.random()*2)-1)
        Vy.append((random.random()*2)-1)
        col.append(0.23)
    Points.append(X)
    Points.append(Y)
    Points.append(Vx)
    Points.append(Vy)
    Points.append(col)
    return Points

def X_by_i(i):
    X = []
    for q in range(100):
        X.append(Points[0][q]+Points[2][q]/100)
        if(X[q] > 2):
            X[q] = X[q]-2
        if(X[q] < 0):
            X[q] = X[q]+2
    Points[0] = X
    return X

def Y_by_i(i):
    Y = []
    for e in range(100):
        Y.append(Points[1][e] + Points[3][e] / 100)
        if (Y[e] > 2):
            Y[e] = Y[e] - 2
        if (Y[e] < 0):
            Y[e] = Y[e] + 2
    Points[1] = Y
    return Y

def long(p1,p2,x,y):
    a = (x[p2]-x[p1])**2
    b = (y[p2]-y[p1])**2
    return (a+b)**0.5

def colours(i,x,y):
    man = 0
    for o in range(100):
        for j in range(100):
            if (long(o,j,x,y) < 0.01) and ((Points[4][o]==1 and Points[4][j]!=1) or (Points[4][j]==1 and Points[4][o]!=1)):
                Points[4][o] = 1
                Points[4][j] = 1
                man = man+1
    if i<500:
        Results.append(Results[i-1]+man)
        print(Results[i])
    return Points[4]


Points = get_points()
Points[4][random.randint(0,99)]=1
Results = [1]

fig = plt.figure()
ax = fig.add_subplot(111, xlim=(0,2), ylim=(0,2))
particles = ax.scatter([], [], c=[], s=25, cmap="hsv", vmin=0, vmax=1)

def animate(i):
    x = X_by_i(i)
    y = Y_by_i(i)
    t = np.array(colours(i,x,y))
    k = np.array(x + y).reshape(100, 2)
    particles.set_offsets(k)
    particles.set_array(t)
    return particles,

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate,frames=400, interval=20, blit=True)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html
#anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()
f = open('results.txt', 'w')
for h in range(len(Results)):
    f.write(str(Results[h])+' ')
f.close()