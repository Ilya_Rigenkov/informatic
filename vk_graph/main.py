import time
import requests
from auauth_vk import token, user_id
import pickle



class VK():
    def __init__(self, token, user_id, version=5.52):
        self.user_id = user_id
        self.token = token
        self.version = version

    def get_friends_by_id(self, id, fields):
        get_str = 'https://api.vk.com/method/friends.get?v={version}&access_token={token}&user_id={id}&fields={fields}'.format(version=self.version, token=self.token, fields=','.join(fields), id=id)
        k = requests.get(get_str).json()
        if 'error' in k.keys():
            print(k)
            return []
        list_value = k["response"]["items"]
        return list_value

    def get_users_info_by_id(self, id):
        k = requests.get('https://api.vk.com/method/users.get?user_id={id}&v={version}'.format(version=self.version, id=id), proxies=proxies).json()


if __name__ == '__main__':



    fields = [
        'nickname',
        'domain',
        'sex',
        'bdate',
        'city',
        'country',
        'timezone',
        'photo_50',
        'photo_100',
        'photo_200_orig',
        'has_mobile',
        'contacts',
        'education',
        'online',
        'relation',
        'last_seen',
        'status',
        'can_write_private_message',
        'can_see_all_posts',
        'can_post',
        'universities'
    ]



    vk = VK(token, user_id)

    vk_friends = {}
    vk_friends[user_id] = vk.get_friends_by_id(user_id, fields)

    for i, user in enumerate(vk_friends[user_id]):
        print(i, user['id'])
        vk_friends[user['id']] = vk.get_friends_by_id(user['id'], fields)
        time.sleep(1)

    print(vk_friends)

    with open('vk_friends.pickle', 'wb') as f:
        pickle.dump(vk_friends, f)