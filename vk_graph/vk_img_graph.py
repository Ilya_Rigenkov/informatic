import pickle
import networkx as nx
import matplotlib.pyplot as plt
from datetime import datetime
import operator

if __name__ == '__main__':
    with open('vk_friends.pickle', 'rb') as f:
        vk_friends = pickle.load(f)

    user_id =184184460

    users = vk_friends.keys()

    k = {}
    for key in vk_friends.keys():
        k[key] = []
        for user in vk_friends[key]:
        #if user['id'] in users:
            k[key].append(user['id'])

    # for key in k.keys():
    #     print(key, k[key])

    graph = nx.from_dict_of_lists(k)
    plt.figure(figsize=(19, 19), dpi=450, )
    nx.draw(graph, node_size=100, cmap=True)
    plt.savefig("%s graph.png" % datetime.now().strftime('%H_%M_%S %d-%m-%Y'))