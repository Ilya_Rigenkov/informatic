import matplotlib.pyplot as plt

def predators(n0,z0,k0,D,l,Bz,Bn,year,total_years):   #модель хищник-жертва, для жертв используется модель ограниченного роста, для хищников модель "вымирания"
    n1 = ((1+(1 - (n0/l))*k0)*n0) - (Bn*n0*z0)        #при этом при встрече щуки с жертвой, с некоторой вероятностью она поедает её, при этом может родиться новая щука(???)
    z1 = (1 - D)*z0 + Bz*n0*z0                        #На графике модели можно наблюдать затухающие колебания численности обоих видов, со временем численность стабилизируется и модель не может
    coels.append(n1)                                  #считаться достаточно точной
    shuki.append(z1)
    if year < total_years:
        predators(n1,z1,k0,D,l,Bz,Bn,year+1,total_years)

coels = []
shuki = []
L = 100
N0 = 50
Z0 = 10
D = 0.8
bn = 0.01
bz = 0.012
k = 0.5

predators(N0,Z0,k,D,L,bz,bn,0,10)
fig = plt.figure()
fig = plt.plot(coels)
fig = plt.plot(shuki)
plt.show()