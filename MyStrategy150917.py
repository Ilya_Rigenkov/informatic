#Мой "проект" создан по мотивам небезызвестной игры XCOM2(пошаговая стратегия, в которой
#люди сражаются против пришельцев). Я создал некоторую аналогию.
#Родительский класс - Hero - любой персонаж в игре.
#Дочерние классы : Solder(солдат), Sectoid(пришелец который может пользоваться пси-силой)
#и Viper(пришелец с большой лазерной пушкой). Все персонажи могут стрелять из оружия, исцеляться,
#менять оружие и умирать(неожиданно). Так-же они имеют метод status(), который выводит информацию о персонаже
#(текущее здоровье и волю)






import random as rnd
class BattleSolderVsSectoid():#Симуляция битвы двух персонажей(первоначально солдата и сектоида),
    # где один боец вооружается дробовиком на 4 ход, а другой старается использовать пси-силу
    def __init__(self):
        self.commando = Solder()
        self.alien = Sectoid()
        i = 1
        while (self.alien.dead==0) and (self.commando.dead==0):
            if(i % 3==0):
                self.alien.RegenHealth()
                self.commando.RegenHealth()
                self.alien.status()
                self.commando.status()
            self.commando.WeaphonAttack(self.alien)
            q = rnd.random()
            if(q > 0.5):
                self.alien.MindAttack(self.alien, self.commando)
            else:
                self.alien.WeaphonAttack(self.commando)
            if(i==4):
                self.commando.SetWeaphon(Shotgun())
            i = i + 1
class Hero():
    def __init__(self):
        self.health = None  #здоровье персонажа
        self.volition = None #воля персонажа(влияет за силу и защиту от пси-атак)
        self.weaphon = None  #текущее оружие персонажа
        self.psyonicforce = None  #пси-способность персонажа
        self.dead = None
    def RegenHealth(self):   #позволяет персонажу восстанавливать здоровье
        self.health += rnd.randint(10,20)
    def status(self):
        raise NotImplementedError()
    def MindAttack(self, you, target):#провести пси-атаку
        self.psyonicforce.Effect(you, target)
    def WeaphonAttack(self, target):  #выстрелить из оружия
        self.weaphon.Shot(target)
    def SetWeaphon(self, weaphon):   #сменить оружие
        self.weaphon = weaphon
    def death(self):        #Уметерь
        raise NotImplementedError()


class Weaphon():     #Интерфейс оружия
    def Shot(self,target):
        raise NotImplementedError()
class PsyonicForce:    #Интерфейс пси-силы
    def Effect(self, you, target):
        raise NotImplementedError()


#Все х-тики оружия вынесены в метод __init__ для более удобных балансных правок
class Pistol(Weaphon): #Пистолет - слабое оружие без особенностей
    def __init__(self):
        self.damage = 15  #урон
        self.aim = 0.8    #меткость(шанс попасть в цель)
    def Shot(self, target):
        shot = rnd.random()
        if (shot < self.aim):
            target.health = target.health - self.damage
            print('Ты попал из пистолета и нанес {0} урона.'.format(self.damage))
            if (target.health<0):
                target.death()
        else:
            print("Твои пистолетные выстрелы не достали врага")
#Дробовик - оружие, которое наносит случайный урон и имеет шанс на критическое попадание
class Shotgun(Weaphon):
    def __init__(self):
        self.criticaldamage = 3  #Модификатор крит. урона
        self.criticalchance = 0.2
        self.aim = 0.9
        self.mindamage = 10
        self.maxdamage = 20
    def Shot(self, target):
        shot = rnd.random()
        damage = rnd.randint(self.mindamage, self.maxdamage)
        if(shot < self.aim):
            if(shot < self.criticalchance):
                damage = damage * self.criticaldamage
                print('Крит!!!')
            target.health = target.health - damage
            print('Ты попал из дробовика, нанеся {0} урона.'.format(damage))
            if (target.health < 0):
                target.death()
        else:
            print('Ты сумел промахнуться даже из дробовика...')
#Лазер - слабое вспомогательное оружие сектоида
class SmallLaser(Weaphon):
    def __init__(self):
        self.damage = 10
        self.aim = 0.7
    def Shot(self, target):
        shot = rnd.random()
        if (shot < self.aim):
            target.health = target.health - self.damage
            print('Сектоид попал лазером и нанес {0} урона.'.format(self.damage))
            if (target.health<0):
                target.death()
        else:
            print('Сектоид промахивается.')
#Большой лазер становиться мощнее при попадании по врагу
class BigLaser(Weaphon):
    def __init__(self):
        self.basicdamage = 25
        self.damageboost = 10
        self.aim = 0.65
    def Shot(self, target):
        shot = rnd.random()
        if (shot < self.aim):
            target.health = target.health - self.basicdamage
            print('Вайпер попадает в тебя, нанося {0} урона и увеличивает заряд своего оружия'.format(self.basicdamage))
            self.basicdamage = self.basicdamage + self.damageboost
            if (target.health<0):
                target.death()
        else:
            print("Вайпер промахивается")

#Если персонаж без пси-способностей применил пси-атаку, то он просто выстрелит из своего оружия
class NoMindAttacks(PsyonicForce):
    def Effect(self, you, target):
        print("Нет пси-способностей, используй свое оружие!")
        you.WeaphonAttack(target)
#Пси-атака сектоида, наносящяя урон в зависимости от разницы воли сектоида и цели. При удачной атаке
#воля сектоида уменьшается, при неудачной - увеличивается
class MentalDrill(PsyonicForce):
    def __init__(self):
        self.basicdamage = 0.5 #урон за 1 ед. разности
        self.feedback = 10     #коэфф. увеличения\уменьшения воли
        self.feedbackRate = 2  #увеличивает прибавку к воле при неудачной атаке
    def Effect(self, you, target):
        if(you.volition > target.volition):
            target.health = target.health - self.basicdamage*(you.volition - target.volition)
            print('Сектоид терзает твой разум и наносит {0} урона.'.format(self.basicdamage*(you.volition - target.volition)))
            you.volition = you.volition - self.feedback
            if (target.health < 0):
                target.death()
        else:
            print('Пси-атака сектоида не удалась, но его концентрация повысилась...')
            you.volition = you.volition + self.feedbackRate*self.feedback


class Solder(Hero):
    def __init__(self):
        self.health = 100
        self.volition = rnd.randint(50,150)
        self.weaphon = Pistol()
        self.psyonicforce = NoMindAttacks()
        self.dead = 0
    def status(self):
        print('Cолдат')
        print(self.health,' здоровья')
        print("Воля - ", self.volition)
    def death(self):
        print('Вот и погиб бравый солдат')
        self.dead = 1
class Sectoid(Hero):
    def __init__(self):
        self.health = 75
        self.volition = rnd.randint(75,200)
        self.weaphon = SmallLaser()
        self.psyonicforce = MentalDrill()
        self.dead = 0
    def status(self):
        print("Сектоид злобно смотрит.")
        print("Здоровье - ", self.health)
        print("Воля - ", self.volition)
    def death(self):
        print("Сектоид умирает, сьеживаясь, становясь похожим на насекомого...")
        self.dead = 1
class Viper(Hero):
    def __init__(self):
        self.health = 150
        self.volition = rnd.randint(50,150)
        self.weaphon = BigLaser()
        self.psyonicforce = NoMindAttacks()
        self.dead = 0
    def status(self):
        print('Вайпер шипит, готовясь к нападению')
        print('Вайпер имеет ', self.health, ' здоровья')
        print("Его воля - ", self.volition)
    def death(self):
        print('Вайпер погибает, издавая глухое шипение')
        self.dead = 1


if __name__ == '__main__':
    a = BattleSolderVsSectoid()
