import random

class State():
    def GetAmount(self):
        raise NotImplementedError()
    def Refill(self):
        raise NotImplementedError()
class Proxy(State):
    def __init__(self):
        self.realObject = None
    def GetAmount(self):
        if (self.realObject==None):
            self.realObject = GumballMachine()
        self.realObject.GetAmount()
    def Refill(self):
        if (self.realObject==None):
            self.realObject = GumballMachine()
        self.realObject.Refill()
class GumballMachine(State):
    def __init__(self):
        self.amount = random.randint(1,200)
    def GetAmount(self):
        print(self.amount)
    def Refill(self):
        self.amount = random.randint(1,200)

if __name__ == '__main__':
    a = Proxy()
    a.GetAmount()
    a.Refill()
    a.GetAmount()
    b = Proxy()
    b.GetAmount()