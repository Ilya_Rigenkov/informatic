n,m=map(int, input().split())
A = [0 for q in range(n)]
for q in range(m):
    i,j = map(int, input().split())
    A[i-1] = A[i-1] + 1
    A[j-1] = A[j-1] + 1
for q in range(len(A)):
    print(A[q], end=' ')