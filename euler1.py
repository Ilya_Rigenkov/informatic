import matplotlib.pyplot as plt
x = 0
dx = 0.1
Y_true = [x]
Y_euler = [x]
for i in range(0,20):
    x = x + dx
    Y_true.append(x**2)
    Y_euler.append(Y_euler[i-1] + 2*x*dx)

fig = plt.figure()
fig = plt.plot(Y_true)
fig = plt.plot(Y_euler)
plt.show()