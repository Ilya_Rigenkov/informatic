import requests as r
import urllib
import os
import argparse

def normal_url_search(text,start_index):
    end = text.find(' ',start_index)   #находим первый пробел, находящийся за url картинки
    liter = text.rfind('.',start_index,end)  #находим последнюю точку в нашем url (перед форматом)
    end = liter + 4
    while(start_index!=-1):
        if (text[start_index]=='.') or (text[start_index]=='h') or (text[start_index]=='/'):    #ищем начало url картинки
            start = start_index
            start_index = -1
            return start, end
        else:
            start_index = start_index + 1

def normal_url(url_site,url_img):       #составляем полный url картинки из url сайта и url самой картинки
    img_url1=url_img.split('/')
    if img_url1[0] == ('http:' or 'https:'):
        return url_img
    else:
        return urllib.parse.urljoin(url_site, url_img)

def ing_url_from_site(site):    #Находим все картинки на сайте
    text=r.get(site).text
    i = text.find('<img')
    while (i!= -1):
        i = text.find('src=',i+1)
        if (i!=-1):
            start,end = normal_url_search(text,i)
            url_img.append(text[start:end])  #Дополняем список из всех url картинок с сайта

def name_by_url(url):
    split = url.split('/') #Разбиваем url на части, в последней из которых содержется имя и расширение
    name = split[len(split)-1]
    name = name.split('.')  #Разбиваем последнюю часть на имя и расширение
    name_format = name[1]
    name = name[0]
    if name in names.keys(): # names - словарь, где имена-ключи, а значения- кол-во повторений изобр. с одинаковыми названиями
        names[name]+=1
    else:
        names.setdefault(name,0)
    if names[name]==0:
        return name+'.'+name_format
    else:
        return name+'_'+str(names[name])+'.'+name_format    #Формируем имя

def ing_path(path,name):    #Функция, возвращающая полный путь файла
    return os.path.join(path,name)

def save_img_by_url(url, path):    #Функция, которая загружает изображения
    o = r.get(url, stream=True)
    if o.status_code == 200:
        with open(path, 'wb') as f:
            for chunk in o:
                f.write(chunk)

parser = argparse.ArgumentParser()    #Добавляем возможность работать с параметрами
parser.add_argument("site", help='Url сайта, откуда необходимо скачать картинки')
parser.add_argument('path', help='Адрес директории, куда будут загружены изображения')
args = parser.parse_args()

site = args.site
path = args.path

url_img = []
names = {}
ing_url_from_site(site)     #Самая СЛОЖНАЯ часть кода(нет). Запускаем все функции в правильном порядке
for i in range(len(url_img)):
    url = normal_url(site,url_img[i])
    name = name_by_url(url)
    img_path = ing_path(path,name)
    save_img_by_url(url, img_path)