
visited = set()
def deep_search(point):
    visited.add(point)
    for i in range(len(A)):
        if A[point][i] == 1 and i not in visited:
            deep_search(i)

n,s = map(int, input().split())
A = []

for i in range(n):
    A.append([int(j) for j in input().split()])

deep_search(s-1)
print(len(visited))