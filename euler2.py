import matplotlib.pyplot as plt
x = 0
dx1 = 0.1
dx2 = 0.01
dx3 = 0.001
Y_true1 = [5]
Y_euler1 = [5]
for i in range(0,50):
    x = x + dx1
    Y_true1.append(3*(x**2)+5*x+5)
    Y_euler1.append(Y_euler1[i-1] + (6*x+5)*dx1)
x = 0
Y_true2 = [5]
Y_euler2 = [5]
for i in range(0,500):
    x = x + dx2
    Y_true2.append(3*(x**2)+5*x+5)
    Y_euler2.append(Y_euler2[i-1] + (6*x+5)*dx2)
x = 0
Y_true3 = [5]
Y_euler3 = [5]
for i in range(0,5000):
    x = x + dx3
    Y_true3.append(3*(x**2)+5*x+5)
    Y_euler3.append(Y_euler3[i-1] + (6*x+5)*dx3)

fig = plt.figure()
fig = plt.plot(Y_true1)
fig = plt.plot(Y_euler1)
fig = plt.plot(Y_true2)
fig = plt.plot(Y_euler2)
fig = plt.plot(Y_true3)
fig = plt.plot(Y_euler3)
plt.show()