import matplotlib.pyplot as plt

def euler(r,To,ts):
    T=[To]
    for i in range(150):
        dT = -r*(T[i]-ts)
        T.append(T[i]+dT*0.1)
    return T

true_results = [83,77.7,75.1,73,71.1,69.4,67.8,66.4,64.7,63.4,62.1,61,59.9,58.7,57.8,56.6]
r = 0.042
Ts = 22

K = euler(r,true_results[0],Ts)
fig = plt.figure()
fig = plt.plot(true_results)
fig = plt.plot(K[::10])
plt.show()