import matplotlib.pyplot as plt

def infine(n0,k,year,total_years):   #модель неограниченного роста
    n1 = (1+k)*n0
    Curve1.append(n1)
    if year < total_years:
        infine(n1,k,year+1,total_years)

def limited(n0,k0,l,year,total_years):   #модель ограниченного роста, чем ближе популяция к мах. значению, тем меньше коэфф
    n1 = (1+(1 - (n0/l))*k0)*n0
    Curve2.append(n1)
    if year < total_years:
        limited(n1,k0,l,year+1,total_years)

def Sotlov(n0,k0,l,R,year,total_years):   #модель ограниченного роста, где каждый период времени популяция уменьшается на константную величину R
    n1 = ((1+(1 - (n0/l))*k0)*n0) - R
    Curve3.append(n1)
    if year < total_years:
        Sotlov(n1,k0,l,R,year+1,total_years)

N0 = 100
K0 =0.5
L = 1000
R = 40

Curve1 = []
infine(N0,K0,0,10)
Curve2 = []
limited(N0,K0,L,0,10)
Curve3 = []
Sotlov(N0,K0,L,R,0,10)

fig = plt.figure()
fig = plt.plot(Curve1)
fig = plt.plot(Curve2)
fig = plt.plot(Curve3)
plt.show()